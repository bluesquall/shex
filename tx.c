// shextx - serial hex transmitter

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>


struct termios opts = {
    .c_iflag = 0,
    .c_oflag = 0,
    .c_cflag = B115200 | CS8 | CLOCAL | CREAD,
    .c_lflag = 0,
    .c_cc[VMIN] = 0,
    .c_cc[VTIME] = 0
};


int main( int argc, char * argv[] ) {
    int8_t i = 0;
    char device[16] = "/dev/ttyUSB0";

    FILE * fp;

    if( argc < 3 ) {
        fprintf( stderr, "usage: " );
        exit( EXIT_FAILURE );
    }

    strcpy( device, argv[1] );

    fp = fopen( device, "wb" );
    if( NULL == fp ) {
        perror( "fopen()" );
        fprintf( stderr, "could not open device: %s\n", device );
        exit( EXIT_FAILURE );
    }

    if ( -1 ==  tcsetattr( fileno( fp ), TCSAFLUSH, &opts ) ) {
        perror( "tcsetattr()" );
        fprintf( stderr, "trouble setting termios attributes\n" );
        fclose( fp );
        exit( EXIT_FAILURE );
    }

    for( int j = 2; j < argc; j++ ) {
        if( 1 == sscanf( argv[j], "%02hhX", &i ) ) {
            if( EOF == fputc( i, fp ) ) {
                perror( "fputc()" );
                // fprintf( stderr, "\n%s\n", explain_fputc(i, fp) );
                fclose( fp );
                exit( EXIT_FAILURE );
            } else {
                printf( " %02hhX", i );
            }
        } else {
            fprintf( stderr, "\nfailed reading arg %d: %s\n", j, argv[j] );
            fclose( fp );
            exit( EXIT_FAILURE );
        }
    }
    printf( "\n" );
    fclose( fp );
    exit( EXIT_SUCCESS );
}
