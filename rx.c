// shexrx - serial hex receiver

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>


struct termios opts = {
    .c_iflag = 0,
    .c_oflag = 0,
    .c_cflag = B115200 | CS8 | CLOCAL | CREAD,
    .c_lflag = 0,
    .c_cc[VMIN] = 0,
    .c_cc[VTIME] = 0
};

int main( int argc, char * argv[] ) {
    int8_t i = 0;
    int8_t vmin = 1;
    int8_t vtime = 1;
    char device[16] = "/dev/ttyUSB0";

    FILE * fp;

    if( argc < 4 ) {
        fprintf( stderr, "usage: " );
        exit( EXIT_FAILURE );
    }

    strcpy( device, argv[1] );
    vmin = atoi( argv[2] );
    vtime = atoi( argv[3] );

    fp = fopen( device, "r+b" );
    if( NULL == fp ) {
        perror( "fopen()" );
        fprintf( stderr, "could not open device: %s\n", device );
        exit( EXIT_FAILURE );
    }

    opts.c_cc[VMIN] = vmin;
    opts.c_cc[VTIME] = vtime;

    if ( -1 ==  tcsetattr( fileno( fp ), TCSAFLUSH, &opts ) ) {
        perror( "tcsetattr()" );
        fprintf( stderr, "trouble setting termios attributes\n" );
        fclose( fp );
        exit( EXIT_FAILURE );
    }

    for( int j = 0; j < vmin; j++ ) {
        i = fgetc( fp );
        if( feof( fp ) ) {
            printf( "\t[EOF]" );
            break;
        } else {
            printf( " %02hhX", i );
        }
    }
    printf( "\n" );
    fclose( fp );
    exit( EXIT_SUCCESS );
}
