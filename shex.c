// shex - serial hex streamer

// #include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>


#define SOH '\x01'
#define ETX '\x03'
#define LF '\x0A'
#define CR '\x0D'
#define DLE '\x10'
#define DEL '\x7F'


struct termios opts = {
    .c_iflag = 0,
    .c_oflag = 0,
    .c_cflag = B115200 | CS8 | CLOCAL | CREAD,
    .c_lflag = 0,
    .c_cc[VMIN] = 0,
    .c_cc[VTIME] = 0
};

int main( int argc, char * argv[] ) {
    char device[16] = "/dev/ttyUSB0";

    FILE * fp;

    if( argc >= 2 ) strcpy( device, argv[1] );
    if( argc >= 3 ) {
        fprintf( stderr, "usage: " );
        exit( EXIT_FAILURE );
    }

    fp = fopen( device, "r+b" );
    if( NULL == fp ) {
        perror( "fopen()" );
        fprintf( stderr, "could not open device: %s\n", device );
        exit( EXIT_FAILURE );
    }

    cfmakeraw( &opts );
    if ( -1 ==  tcsetattr( fileno( fp ), TCSAFLUSH, &opts ) ) {
        perror( "tcsetattr()" );
        fprintf( stderr, "trouble setting termios attributes" );
        exit( EXIT_FAILURE );
    }

    while( 1 <3 ) { // while: one love, one heart
        char c = fgetc( fp );
        switch ( c ) {
            case SOH :
            case LF :
                printf( "\n" );
        }

        printf( "%02hhX", c );

        switch ( c ) {
            case CR :
            case ETX :
                printf( "\n" );
            default :
                printf ( " " );
        }
    }

    fclose( fp );
    exit( EXIT_SUCCESS );
}
